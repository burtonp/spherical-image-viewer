module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);
    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: ['build'],
        ejs: {
            all: {
                options: {
                    // site-wide vars here
                },
                src: ['**/*.ejs', '!node_modules/**/*', '!_*/**/*'],
                dest: 'build',
                expand: true,
                ext: '.html'
            }
        },
        eslint: {
            options: {
                configFile: '.eslintrc.json',
                reset: true
            },
            target: ['public/js/**/*.js']
        },
        uglify: {
            build: {
                files: {
                    'build/js/libpannellum.min.js': ['public/js/libpannellum.js'],
                    'build/js/RequestAnimationFrame.min.js': ['public/js/RequestAnimationFrame.js'],
                    'build/js/pannellum.min.js': ['public/js/pannellum.js']
                }
            }
        },
        sass: {
            dist: {
                files: {
                    'public/css/master.css' : 'public/css/scss/master.scss'
                }
            }
        },
        cssc: {
            build: {
                options: {
                    consolidateViaDeclarations: true,
                    consolidateViaSelectors:    true,
                    consolidateMediaQueries:    true
                },
                files: {
                    'build/css/master.css': 'build/css/master.css'
                }
            }
        },
        cssmin: {
            build: {
                src: 'public/css/master.css',
                dest: 'build/css/master.css'
            }
        },
        watch: {
            html: {
                files: '<%= ejs.all.src %>',
                tasks: ['ejs']
            },
            css: {
                files: 'public/css/scss/**/*.scss',
                tasks: ['buildcss']
            },
            js: {
                files: 'public/js/**/*.js',
                tasks: ['eslint', 'uglify']
            }
        }
    });

    // Helper tasks
    grunt.registerTask('buildcss',  ['sass']);
    grunt.registerTask('buildcss_prod',  ['sass', 'cssc', 'cssmin']);

    // Dev - Builds sass, runs linters, and starts server/watcher
    grunt.registerTask('dev', [
        'buildcss',
        'eslint',
        'watch'
    ]);

    // Prod - Builds and minifies css, runs linters, runs uglify
    grunt.registerTask('prod', [
        'clean',
        'ejs',
        'buildcss_prod',
        'eslint',
        'uglify'
    ]);

    grunt.registerTask('default', ['dev']);
};