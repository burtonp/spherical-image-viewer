# Spherical Panoramic Image Viewer #

This is a POC for a spherical and panoramic image viewer. 

Originally forked from: [Matthew Petroff's](https://github.com/mpetroff) --> [Pannellum](https://github.com/mpetroff/pannellum). 

Implemented Node/express server and grunt build processes. Broke apart the nasty css into a sass architecture for my sanity and managability. The sass follows a ["SMACSS"](https://smacss.com/) pattern.

[NOTES] Refactor .js lib to not use IFrames...[need to look into this more, but shouldn't be too bad] 
* This should just be an encapsulated, configurable, .js module and not need these goofy iframes
* https://bitbucket.org/burtonp/spherical-image-viewer/issues/4/refactor-js-lib

### Project Info ###
* Market: Chicago
* Start Date: 12/28/15
* Hipchat room: ```Spherical Image Viewer```
* Bitbucket: https://bitbucket.org/burtonp/spherical-image-viewer/overview
* Issues: https://bitbucket.org/burtonp/spherical-image-viewer/issues

### Project Dependencies ###
* ```npm```
* ```grunt cli```

### Quick Start ###
* ```npm install```
* ```npm start```
* goto: ```http://localhost:5000```

### Dev Tooling ###
#### ```grunt dev``` ####
* Builds sass
* Runs linters on js and html
* Starts watchers

#### ```grunt prod``` ####
* Builds sass and minifies the resulting css
* Runs linters on js and html
* Runs uglify on js

### Tech Stack ###
#### Pannellum ####
* https://pannellum.org/
* https://github.com/mpetroff/pannellum

### Contribution guidelines ###

* ```TODO```
### Team ###

* @Burtonp
* Will Capellaro
* @thejordan