var express = require('express');
var app = express();
//var cool = require('cool-ascii-faces');

app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {
    response.render('pages/index');
});

app.get('/views/pages/sphericalImageViewer.ejs', function(request, response, next) {
    var configPath = request.param('config');
    //response.send(configPath);

    response.render('pages/sphericalImageViewer');
});

app.get('/cool', function(request, response) {
    response.send("Cool! Routing Works!");
});

app.listen(app.get('port'), function() {
    console.log('Node app is running on port', app.get('port'));
});
